#! /bin/sh
# Read configuration values	
. /etc/diaspora/diaspora-common.conf

# Create an empty public/source.tar.gz for diaspora package
# script/server checks for its existence
su diaspora -s /bin/sh -c 'test -f public/source.tar.gz || touch public/source.tar.gz'

if grep https ${diaspora_conf}
then 
	mkdir -p ${diaspora_ssl_path}
	echo "Copy $SERVERNAME-bundle.pem and $SERVERNAME.key to /etc/diaspora/ssl"
	echo "And reload nginx, run # /etc/init.d/nginx reload"
fi

echo "To stop diaspora, run # /etc/init.d/diaspora stop"
echo "To see the service status, run # /etc/init.d/diaspora status"
echo "To start diaspora service, run # /etc/init.d/diaspora start"

echo "Visit your pod at $ENVIRONMENT_URL"

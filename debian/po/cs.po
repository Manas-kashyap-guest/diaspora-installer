# Czech PO debconf template translation of diaspora-installer.
# Copyright (C) 2015 Michal Simunek <michal.simunek@gmail.com>
# This file is distributed under the same license as the diaspora-installer package.
# Michal Simunek <michal.simunek@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: diaspora-installer 0.3\n"
"Report-Msgid-Bugs-To: diaspora-installer@packages.debian.org\n"
"POT-Creation-Date: 2017-04-27 12:48+0530\n"
"PO-Revision-Date: 2015-04-03 10:15+0200\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid "Host name for this instance of Diaspora:"
msgstr "Název hostitele pro tuto instanci Diaspora:"

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"Please choose the host name which should be used to access this instance of "
"Diaspora."
msgstr ""
"Zvolte si prosím název, který se má používat pro přístup k této instanci "
"Diaspora."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"This should be the fully qualified name as seen from the Internet, with the "
"domain name that will be used to access the pod."
msgstr ""
"Měl by to být plně kvalifikovaný název viditelný z Internetu a s názvem "
"domény, který se bude používat pro přístup k podu."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"If a reverse proxy is used, give the hostname that the proxy server responds "
"to."
msgstr ""
"Pokud se používá reverzní proxy, zadejte název hostitele, na kterém proxy "
"server odpovídá."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"This host name should not be modified after the initial setup because it is "
"hard-coded in the database."
msgstr ""
"Název hostitele by se po úvodním nastavení neměl měnit, protože je pevně "
"zapsán v databázi."

#. Type: note
#. Description
#: ../diaspora-common.templates:2001
msgid "PostgreSQL application password"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:2001
msgid ""
"You can leave the PostgreSQL application password blank, as the \"ident\" "
"authentication method is used, allowing the diaspora user on the system to "
"connect to the Diaspora database without a password."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid "Enable https?"
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Enabling https means that an SSL/TLS certificate is required to access this "
"Diaspora instance (as Nginx will be configured to respond only to https "
"requests). A self-signed certificate is enough for local testing (and can be "
"generated using, for instance, the package easy-rsa), but will not be "
"accepted for federation with other Diaspora pods."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Some certificate authorities like Let's Encrypt (letsencrypt.org), StartSSL "
"(startssl.com) offer free SSL/TLS certificates that work with Diaspora; "
"however, certificates provided by CAcert will not work with Diaspora."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Nginx must be reloaded after the certificate and key files are made "
"available at /etc/diaspora/ssl. letsencrypt package may be used to automate "
"interaction with Let's Encrypt to obtain a certificate."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"You can disable https if you want to access Diaspora only locally or you "
"don't want to federate with other diaspora pods."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid "Use Let's Encrypt?"
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Symbolic links to certificate and key created using letsencrypt package (/"
"etc/letencrypt/live) will be added to /etc/diaspora/ssl if this option is "
"selected."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Otherwise, certificate and key files have to be placed manually to /etc/"
"diaspora/ssl directory as '<host name>-bundle.crt' and '<host name>.key'."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Nginx will be stopped, if this option is selected, to allow letsencrypt to "
"use ports 80 and 443 during domain ownership validation and certificate "
"retrieval step."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Note: letsencrypt does not have a usable nginx plugin currently, so "
"certificates must be renewed manually after 3 months, when current "
"letsencrypt certificate expire. If you choose this option, you will also be "
"agreeing to letsencrypt terms of service."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:5001
msgid "Email address for letsencrypt updates:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:5001
msgid "Please provide a valid email address for letsencrypt updates."
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid "Backup your database"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"This upgrade includes long running migrations that can take hours to "
"complete on large pods. It is adviced to take a backup of your database."
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"Commands to backup and restore database is given below (run as root user):"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"# su postgres -c 'pg_dump diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"# su postgres -c 'psql -d diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"
msgstr ""

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid "Third party services to be enabled: "
msgstr ""

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid "Diaspora can connect with different services."
msgstr ""

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid ""
"When a diaspora instance is connected to a third party service,  it allows "
"any user of this diaspora instance to link their account on that service to "
"their diaspora account and send their updates to that service if they choose "
"the service when publishing a post."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:8001
msgid "Facebook App ID:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:8001
msgid "Give your Facebook App ID. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:9001
msgid "Facebook Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:9001
msgid "Give your Facebook Secret. This can not be blank."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:10001
msgid "Twitter Key:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:10001
msgid "Give your Twitter Key. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:11001
msgid "Twitter Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:11001
msgid "Give your Twitter Secret. This can not be blank."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:12001
msgid "Tumblr Key:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:12001
msgid "Give your Tumblr Key. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:13001
msgid "Tumblr Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:13001
msgid "Give your Tumblr Secret. This can not be blank."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:14001
msgid "Wordpress Client ID:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:14001
msgid "Give your Wordpress Client ID. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:15001
msgid "Wordpress Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:15001
msgid "Give your Wordpress Secret. This can not be blank."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:16001
msgid "Remove all data?"
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:16001
msgid ""
"This will permanently remove all data of this Diaspora instance such as "
"uploaded files and any customizations in homepage."
msgstr ""

#! /bin/sh
set -e

diaspora_home=/usr/share/diaspora

# Keep it in sync with postrm
# Diaspora needs to write to these files and directories, but debian policy
# does not allow packages to write to /usr, so we add these links so diaspora
# can write to /var instead
diaspora_symlinks_list="Gemfile.lock log tmp public app/assets bin/bundle vendor/bundle config config/oidc_key.pem"
# These directories have symlinks in them, which is handled by diaspora-common
# so we should preserver these links
diaspora_symlinks_dirs="app bin vendor db"

# Fix bin symlink set by earlier versions
if test -L ${diaspora_home}/bin
then
    rm -rf ${diaspora_home}/bin
fi

# Backup the previous version
# Just keep the modified files/directories
# We need this to remove files removed upstream
backup() {
    backup_suffix=$(openssl rand -hex 4)
    backup_dir=${diaspora_home}/.backup.${backup_suffix}
    mkdir  ${backup_dir}
    mv ${diaspora_home}/* ${backup_dir}

    for i in ${diaspora_symlinks_dirs}; do
      mkdir ${diaspora_home}/$i
    done

    for i in ${diaspora_symlinks_list}; do
      test -e ${backup_dir}/$i && mv ${backup_dir}/$i ${diaspora_home}/$i
    done
}
 
case "$1" in
    upgrade)
            echo "Stopping diaspora..."
            invoke-rc.d diaspora stop
            echo "Making a backup of ${diaspora_home}..."
            backup || true
        ;;
    abort-upgrade|install)
        ;;
    *)
        echo "preinst called with unknown argument \`$1'" >&2
        exit 1
        ;;
esac

#DEBHELPER#

exit 0

Source: diaspora-installer
Section: net
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>, Joseph Nuthalapati <njoseph@riseup.net>
Build-Depends: debhelper (>= 11),
               gem2deb,
               po-debconf
Standards-Version: 4.1.3
Vcs-Git: https://salsa.debian.org/ruby-team/diaspora-installer.git
Vcs-Browser: https://salsa.debian.org/ruby-team/diaspora-installer
Homepage: https://wiki.debian.org/Diaspora
XS-Ruby-Versions: all

Package: diaspora-installer
Architecture: all
Section: contrib/net
XB-Ruby-Versions: ${ruby:Versions}
Depends: build-essential,
         diaspora-common (= ${source:Version}),
         ghostscript,
         imagemagick,
         libcurl4-openssl-dev,
         libmagickwand-dev,
         libpq-dev,
         libssl-dev,
         libxml2-dev,
         libxslt-dev,
         libffi-dev,
         zlib1g-dev,
         ruby (>= 2.2) | ruby-interpreter,
         ruby-dev,
         wget,
         rsync,
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: diaspora
Replaces: diaspora
Description: distributed social networking service - installer
 Diaspora (currently styled diaspora* and formerly styled DIASPORA*) is a free
 personal web server that implements a distributed social networking service.
 Installations of the software form nodes (termed "pods") which make up the
 distributed Diaspora social network.
 .
 Diaspora is intended to address privacy concerns related to centralized
 social networks by allowing users to set up their own server (or "pod") to
 host content; pods can then interact to share status updates, photographs,
 and other social data. It allows its users to host their data with a
 traditional web host, a cloud-based host, an ISP, or a friend. The framework,
 which is being built on Ruby on Rails, is free software and can be
 experimented with by external developers.
 .
 Learn more about Diaspora at http://diasporafoundation.org
 .
 This dummy package downloads diaspora (also pulling in runtime
 dependencies as rubygems) and configures it to use PostgreSQL and
 Nginx.
 .
 Unlike the normal Debian package, this package installs exact versions of the
 dependencies supported by upstream.

Package: diaspora-installer-mysql
Architecture: all
Section: contrib/ruby
XB-Ruby-Versions: ${ruby:Versions}
Depends: dbconfig-mysql | dbconfig-no-thanks,
         default-libmysqlclient-dev,
         default-mysql-server | virtual-mysql-server,
         diaspora-installer (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: distributed social networking service - installer (with MySQL)
 Diaspora (currently styled diaspora* and formerly styled DIASPORA*) is a free
 personal web server that implements a distributed social networking service.
 Installations of the software form nodes (termed "pods") which make up the
 distributed Diaspora social network.
 .
 Diaspora is intended to address privacy concerns related to centralized
 social networks by allowing users to set up their own server (or "pod") to
 host content; pods can then interact to share status updates, photographs,
 and other social data. It allows its users to host their data with a
 traditional web host, a cloud-based host, an ISP, or a friend. The framework,
 which is being built on Ruby on Rails, is free software and can be
 experimented with by external developers.
 .
 Learn more about Diaspora at http://diasporafoundation.org
 .
 This dummy package downloads diaspora (also pulling in runtime
 dependencies as rubygems) and configures it to use MySQL and  Nginx.
 .
 Unlike the normal Debian package, this package installs exact versions of the
 dependencies supported by upstream.

Package: diaspora-common
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Pre-Depends: ca-certificates
Depends: adduser,
         lsb-base,
         bc,
         curl,
         dbconfig-pgsql | dbconfig-mysql | dbconfig-no-thanks,
         default-mta | mail-transport-agent,
         net-tools,
         nginx | httpd,
         nodejs,
         postgresql | default-mysql-server | virtual-mysql-server,
         rake,
         redis-server (>= 2:2.8),
         ruby | ruby-interpreter,
         ruby-rspec,
         sudo,
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: diaspora-installer (<= 0.7.6.1~)
Replaces: diaspora-installer (<= 0.7.6.1~)
Suggests: easy-rsa
Description: distributed social networking service - common files
 Diaspora (currently styled diaspora* and formerly styled DIASPORA*) is a free
 personal web server that implements a distributed social networking service.
 Installations of the software form nodes (termed "pods") which make up the
 distributed Diaspora social network.
 .
 Diaspora is intended to address privacy concerns related to centralized
 social networks by allowing users to set up their own server (or "pod") to
 host content; pods can then interact to share status updates, photographs,
 and other social data. It allows its users to host their data with a
 traditional web host, a cloud-based host, an ISP, or a friend. The framework,
 which is being built on Ruby on Rails, is free software and can be
 experimented with by external developers.
 .
 Learn more about Diaspora at http://diasporafoundation.org
 .
 It provides files common for the diaspora and diaspora-installer packages.
